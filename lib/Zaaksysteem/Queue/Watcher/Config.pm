package Zaaksysteem::Queue::Watcher::Config;

use Moose::Role;

use BTTW::Tools;
use Zaaksysteem::InstanceConfig::Model;

=head1 NAME

Zaaksysteem::Queue::Watcher::Config - Configuration role for the queue watcher

=head1 DESCRIPTION

This role, when applied, adds a L</instance_configs> attribute to the
class/instance.

=head1 DEPENDENCIES

=head2 instance_config_model_args

This method should return arguments to be passed to
L<Zaaksysteem::InstanceConfig::Model>, so it can construct our config source.

=cut

requires qw[instance_config_model_args];

sig instance_config_model_args => '=> HashRef';

=head1 ATTRIBUTES

=head2 instance_configs

A convenience attribute that holds a reference to an array of hashrefs.

The value is build via the C<_build_instance_configs> method, which can be
overridden in the consuming class.

=head3 Proxied methods

=over 4

=item all_instance_configs

Returns the array elements as a list

=back

=cut

has instance_configs => (
    is => 'ro',
    isa => 'ArrayRef[HashRef]',
    lazy => 1,
    traits => [qw[Array]],
    builder => '_build_instance_configs',
    handles => {
        all_instance_configs => 'elements'
    }
);

sub _build_instance_configs {
    my $self = shift;

    my $model = Zaaksysteem::InstanceConfig::Model->new(
        $self->instance_config_model_args
    );

    my @instance_configs;

    for my $host ($model->all_instance_hosts) {
        push @instance_configs, $model->get_instance_config($host);
    }

    return \@instance_configs;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE> file.
