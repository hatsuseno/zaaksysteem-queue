package Zaaksysteem::Queue::Watcher;

use MooseX::App::Simple;

use Encode qw[encode_utf8];
use JSON::XS;
use LWP::UserAgent;
use HTTP::Request;

=head1 NAME

Zaaksysteem::Queue::Watcher - Zaaksysteem database notification watcher

=head1 SYNOPSIS

See help/usage

    zs-queue.pl --help

=head1 OPTIONS

=head2 zaaksysteem_config_path

Sets the path to the main zaaksysteem configuration file.

=cut

option zaaksysteem_config_path => (
    is => 'ro',
    isa => 'Str',
    required => 1,
    cmd_aliases => [qw[d]],
    cmd_flag => 'config',
    documentation => 'Path to zaaksysteem.conf with InstanceConfig source config'
);

=head2 reconnect_sleep

Sets the sleep timeout (in seconds) used when reconnecting to the configured
databases

=cut

option reconnect_sleep => (
    is => 'ro',
    isa => 'Int',
    required => 1,
    default => 10,
    documentation => 'Time in seconds to wait before reconnecting after all listeners have died'
);

# Seperate 'with' calls, if provided as a list the dependency resolving breaks
# (::Selector requires ::DB requires ::Config)
with $_ for qw[
    MooseX::Log::Log4perl
    Zaaksysteem::Queue::Watcher::Config
    Zaaksysteem::Queue::Watcher::DB
    Zaaksysteem::Queue::Watcher::Selector
    Zaaksysteem::Queue::Watcher::StatsD
];

=head1 ATTRIBUTES

=head2 json

Convenience attribute that holds a reference to a L<JSON> instance.

=cut

has json => (
    is => 'rw',
    isa => 'JSON::XS',
    default => sub { JSON::XS->new },
    handles => {
        decode_message => 'decode'
    }
);

=head2 user_agent

Convenience attribute that holds a reference to a L<LWP::UserAgent> instance.

=cut

has user_agent => (
    is => 'rw',
    isa => 'LWP::UserAgent',
    default => sub {
        LWP::UserAgent->new(
            env_proxy => 1,
            ssl_opts => {
                verify_hostname => 0,
                SSL_ca_path => '/etc/ssl/certs'
            },
            protocols_allowed => [qw[http https]]
        );
    },
    handles => {
        'request' => 'request'
    }
);

=head1 METHODS

=head2 run

Runs the application in a continuous loop.

Also checks that sockets are ready for listening, and reconnects everything
if no active sockets are found.

=cut

sub run {
    my $self = shift;

    $self->log->info('Starting Zaaksysteem Queue notification watcher');

    # Auto-reap forked message handlers
    local $SIG{ CHLD } = 'IGNORE';

    while (1) {
        $self->log->debug('Main loop iteration, waiting for notifications');

        my $ok = $self->wait_and_dispatch_notifications;

        unless ($ok) {
            $self->statsd->increment('failed_dispatch', 1);
            my $sleep = $self->reconnect_sleep;

            $self->log->info('Wait/dispatch not OK, reconnecting everything');

            if ($sleep) {
                $self->log->info(sprintf(
                    'Waiting %d seconds before reconnecting',
                    $sleep
                ));

                sleep $sleep;
            }

            $self->clear_selector;
            $self->clear_db_handles;
        }
    }
}

=head2 wait_and_dispatch_notifications

Wait for, and dispatch when received, notifications.

This method is the main part of the L</run> loop. It will hold until one or
more notifications are received and dispatches them via L</fork_and_run>.

Database disconnects are also handled by this method. Databases from which
they originate are removed from the list of sockets to listen on.

This is the method you most likely want to use when building your own variant
of the queue listener.

=cut

sub wait_and_dispatch_notifications {
    my $self = shift;

    my $ok;

    for my $handle (map { $self->get_dbh($_) } $self->can_read) {
        $ok = 1 unless defined $ok;

        my $notification = $handle->func('pg_notifies');

        unless (defined $notification) {
            $self->log->warn(sprintf(
                'Received undefined notification from "%s", disconnecting',
                $handle->{ pg_db }
            ));

            $self->remove_socket($handle->{ pg_socket });
            $ok = 0;

            next;
        }

        do {
            my ($name, $pid, $message) = @{ $notification };

            $self->log->info(sprintf(
                'Action notification received from "%s": %s',
                $handle->{ pg_db },
                $message
            ));

            $self->fork_and_run($message);
        } while ($notification = $handle->func('pg_notifies'));
    }

    return $ok;
}

=head2 fork_and_run

This method is called by L</wait_and_dispatch_notifications> when a new
notification event is received. It forks the process, and returns for the
main process. The new child will decoded and process the payload received.

=cut

sub fork_and_run {
    my $self = shift;
    my $message = shift;

    my $cpid = fork;

    unless (defined $cpid) {
        Moose->throw_error('failed to fork');
    }

    return if $cpid;

    my $data = $self->decode_message($message);

    unless (defined $data && exists $data->{ url }) {
        $self->log->warn(sprintf(
            'Payload not found, incomplete, or corrupt; unable to run action'
        ));

        exit 1;
    }

    my $url = delete $data->{ url };

    my $content = encode_utf8($self->json->encode($data));

    my $request = HTTP::Request->new(
        'POST',
        $url,
        [ 'Content-Type' => 'application/json' ],
        $content
    );

    $self->request_start_time($self->statsd->start);
    $self->handle_response($self->request($request));
    $self->statsd->end('request.time', $self->request_start_time);

    exit;
}

=head2 instance_config_model_args

Returns constructor arguments for the configuration role

=cut

sub instance_config_model_args {
    my $self = shift;

    return {
        zaaksysteem_config_path => $self->zaaksysteem_config_path
    };
}

=head2 handle_response

This method is called on every request/response cycle. It's main purpose
is to log errors or success messages.

=cut

sub handle_response {
    my $self = shift;
    my $response = shift;

    my $message;
    my $content = eval {
        $self->decode_message($response->decoded_content)->{ result }[0]
    };

    if ($response->is_error) {
        my $base = sprintf('Error while running action (%s)', $response->status_line);

        if (defined $content) {
            $message = sprintf(
                '%s: exception "%s": %s',
                $base,
                $content->{ type },
                join(', ', @{ $content->{ messages } })
            );
        } else {
            $message = sprintf(
                '%s: %s',
                $base,
                $response->decoded_content
            );
        }

        $self->log->warn($message);

        return;
    }

    if (defined $content) {
        $message = sprintf(
            '%s for item "%s(%s): %s"',
            $content->{ status },
            $content->{ type },
            $content->{ id },
            $content->{ label }
        );

        $self->statsd->increment(sprintf("item.%s.%s", $content->{type},$content->{status}), 1);
        $self->statsd->end(sprintf("item.%s.time", $content->{type}), $self->request_start_time);
    } else {
        $self->statsd->increment('corrupt_item', 1);
        $message = sprintf(
            'Corrupt JSON: %s',
            $response->decoded_content
        );
    }

    my $timing = $self->statsd->milliseconds($self->request_start_time);

    $self->log->info(sprintf(
        'Action response (%s) took %d ms: %s',
        $response->status_line,
        $timing,
        $message
    ));
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
